var chai = require('chai')
var chaihttp = require('chai-http')
const expect = require('chai').expect

chai.use(chaihttp)

describe('Testing', function () { // eslint-disable-line
  it('Servicio Almacenamiento Retorna Complementos', function (done) { // eslint-disable-line 
    chai.request('http://localhost:3000')
      .get('/complementos')
      .end(function (err, res) { // eslint-disable-line 
        if (res !== undefined) {
          expect(res).to.have.status(200)
          console.log(res.body)
        }
        done()
      })
  })

  it('Busca Complemento por ID', function (done) { // eslint-disable-line 
    chai.request('http://localhost:3000')
      .get('/complemento/3')
      .end(function (err, res) { // eslint-disable-line
        if (res !== undefined) {
          expect(res).to.have.status(200)
          console.log(res.body)
        }
        done()
      })
  })

	it('Busca Complemento por ID', function (done) { // eslint-disable-line 
    chai.request('http://localhost:3000')
      .post('/complementoTraducido')
      .end(function (err, res) { // eslint-disable-line
        if (res !== undefined) {
          expect(res).to.have.status(200)
          console.log(res.body)
        }
        done()
      })
  })


  it('Retorna Catalogo', function (done) { // eslint-disable-line
    chai.request('http://localhost:3000')
      .get('/catalogo')
      .end(function (err, res) { // eslint-disable-line
        if (res !== undefined) {
          expect(res).to.have.status(200)
          console.log(res.body)
        }
        done()
      })
  })
  
  it('Traducciones Get Localizaciones', function (done) { // eslint-disable-line
    chai.request('http://localhost/traducciones:3000')
      .get('/localizacion/3')
      .end(function (err, res) { // eslint-disable-line
        if (res !== undefined) {
          expect(res).to.have.status(200)
          console.log(res.body)
        }
        done()
      })
  })
  
  it('Traducciones Get Catalogo', function (done) { // eslint-disable-line
    chai.request('http://localhost/traducciones:3000')
      .get('/complemento/14')
      .end(function (err, res) { // eslint-disable-line
        if (res !== undefined) {
          expect(res).to.have.status(200)
          console.log(res.body)
        }
        done()
      })
  })
  
  it('Traducciones Get Complemento Id', function (done) { // eslint-disable-line
    chai.request('http://localhost/traducciones:3000')
      .get('/complemento/14')
      .end(function (err, res) { // eslint-disable-line
        if (res !== undefined) {
          expect(res).to.have.status(200)
          console.log(res.body)
        }
        done()
      })
  })
  
 it('Traducciones Post AprobarTraduccionCadena', function (done) { // eslint-disable-line
    chai.request('http://localhost/traducciones:3000')
      .post('/AprobarTraduccionCadena')
      .end(function (err, res) { // eslint-disable-line
        if (res !== undefined) {
          expect(res).to.have.status(200)
          console.log(res.body)
        }
        done()
      })
  })
  
  it('Almacenamiento Inserta Complemento', function (done) { // eslint-disable-line
    chai.request('http://localhost/traducciones:3000')
      .post('/sp_almacenamientoInsertaComplemento')
      .end(function (err, res) { // eslint-disable-line
        if (res !== undefined) {
          expect(res).to.have.status(200)
          console.log(res.body)
        }
        done()
      })
  })
  
    it('Almacenamiento Catalogo', function (done) { // eslint-disable-line
    chai.request('http://localhost/traducciones:3000')
      .post('/sp_almacenamientoObtenerCatalogo')
      .end(function (err, res) { // eslint-disable-line
        if (res !== undefined) {
          expect(res).to.have.status(200)
          console.log(res.body)
        }
        done()
      })
  })
  
 it('Almacenamiento Agregar Cadena', function (done) { // eslint-disable-line
    chai.request('http://localhost/traducciones:3000')
      .post('/sp_almacenamientoInsertaTraduccion')
      .end(function (err, res) { // eslint-disable-line
        if (res !== undefined) {
          expect(res).to.have.status(200)
          console.log(res.body)
        }
        done()
      })
  })
  
   it('Almacenamiento Suscripcion', function (done) { // eslint-disable-line
    chai.request('http://localhost/traducciones:3000')
      .post('/sp_almacenamientosuscripcion')
      .end(function (err, res) { // eslint-disable-line
        if (res !== undefined) {
          expect(res).to.have.status(200)
          console.log(res.body)
        }
        done()
      })
  })
  
  
  
})
