const http = require('http')

const clientData = JSON.stringify({
    "clientid" : process.env.CLIENT_ID,
    "password": process.env.CLIENT_PASSWORD
})

const options = {
    host : process.env.JWT_HOST,
    port: process.env.JWT_PORT,
    path: '/post/autorizacion',
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
        'Content-Length': clientData.length
    }
} 

function getToken(req,res,next){

    console.log('getting token....')
    console.log(options);

    const request = http.request(options, res =>{

        const contentType = res.headers['content-type']

        var data = ''

        res.on('data', function(chunk){
            data +=chunk
        }).on('end', function(){
            var response = data
            if(contentType.indexOf('application/json') != -1){
                response = JSON.parse(data);
                if(response.estado == '200')
                    req.token = response.data.token

                console.log('response from JWT: ')
                console.log(response);
                next()
            }
            
        }).on('error', function(err){
            console.error('Ha ocurrido un error: ' + err)
        }).on('uncaughtException', function(err){
            console.error('excepcion ' + err)
        })
    }).on('error', function(err){
        console.error('request ' + err)
    })
    
    request.write(clientData);
    request.end();
}

module.exports = getToken