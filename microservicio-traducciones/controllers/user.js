'use static'

const User = require('../models/user')
const bcrypt = require('bcrypt')

function registerUser(req, res){
    const user = new User({
        nombre : req.body.nombre,
        correo : req.body.correo,
        password: req.body.password,
        locations: req.body.locations
    })
    User.findOne({correo: req.body.correo}, (err,userFound)=>{
        if(err) return res.status(500).send({message: `Error al crear nuevo usuario: ${err}`})
        if(userFound) return res.status(409).send({message: `Ya existe un usuario registrado con el mismo correo ${user.correo}`})
            
        user.save((err)=>{
            if(err) return res.status(500).send({message: `Error al registrar nuevo usuario: ${err}`})
            return res.status(200).send({message : 'Usuario creado correctamente', data : { nombre: user.nombre, correo : user.correo}})
        })
    })
}

function logIn(req, res){
    User.findOne({correo: req.body.correo}, (err,user)=>{
        if(err) return res.status(500).send({message: `Ha ocurrido un error al buscar al usuario: ${err}`})
        if(!user)return res.status(404).send({message: `No existe usuario registrado con el correo ${req.body.correo}`})
        bcrypt.compare(req.body.password, user.password, (err,same)=>{
            if(err)res.status(500).send({message: `Ha ocurrido un error al comparar contraseñas: ${err}`})
            if(same){
                req.user = user
                res.status(200).send({
                    message: 'El usuario ingresado y contraseña son correctos.'
                })
            }
            else res.status(401).send({message: 'Credenciales incorrectas'})
        })
    })
}

function updateLocations(req, res){
    User.findOne({correo: req.body.correo}, (err,user)=>{
        if(err) return res.status(500).send({message: `Ha ocurrido un error al buscar al usuario: ${err}`})
        if(!user)return res.status(404).send({message: `No existe usuario registrado con el correo ${req.body.correo}`})
        user.locations = req.body.locations
        user.save((err)=>{
            if(err) return res.status(500).send({message: `Error al actualizar las localizaciones del usuario ${user.correo}; error: ${err}`})
            return res.status(200).send({message: 'Localizaciones actualizadas correctamente', data: {correo: user.correo, locations: user.locations}})
        })
    })
}

function getLocations(req, res){
    User.findOne({correo: req.params.idUsuario}, (err,user)=>{
        if(err) return res.status(500).send({message: `Ha ocurrido un error al buscar al usuario: ${err}`})
        if(!user)return res.status(404).send({message: `No existe usuario registrado con el correo ${req.body.correo}`})
        res.status(200).send({locations: user.locations})
    })
}

module.exports = {
    registerUser,
    logIn,
    updateLocations,
    getLocations
}