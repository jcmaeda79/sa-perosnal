'use strict';

const express = require('express'); //herramienta que permite crear y configurar un servidor
const app = express.Router();
var con = require('./connection');
var validation = require('./getTokenValidation')
    /**
    * @link http://IP:puerto/post/complemento
    * @description
    * Ruta que interactúan con la bd y guarda el complemento
    */
app.post('/post/complemento',function (request, res){
    const token = request.body.token || request.params.token || request.query.token;
    var validar = validation.tokenValidation(token)
    if(validar != true){
        return res.send(validar);
    }
    var cadena  = request.query.complemento.cadenas;
    var complemento = request.query.complemento;
    var cadenas = "";
    for(var i =0 ; i < cadena.length; i++){                      
        cadenas += cadena[i]+",";
    }    
    var json = new Object();
    function getData(callback){
        con.connecting().query('Call Almacenamiento.sp_almacenamientoInsertaComplemento("'+request.query.nombre+'","'+request.query.correo+'","'+
        complemento.nombre+'","'+complemento.localizacion+'","'+cadenas.substring(0, cadenas.length - 1)+'");',function(err, rows){
            if (err){
                json.estado = "401";
                json.mensaje = "Se produjo un error al insertar un complemento";
            }else{
                json.estado = "200";
                json.mensaje = "Se ha insertado el complemento éxitosamente.";
            }
            callback(null,json);
        });
    }
    getData(function(err, content){
        res.send(content);
    });
});  
    /**
    * @link http://IP:puerto/get/complementos?token=token
    * @description
    * Ruta que interactúan con la bd y devuelve una lista de complementos
    */
app.get('/get/complementos', function (request, res){
    const token = request.body.token || request.params.token || request.query.token;
    var validar = validation.tokenValidation(token)
    if(validar != true){
        return res.send(validar);
    }
    var json = new Object();
    function getData(callback){
        con.connecting().query('Call Almacenamiento.sp_almacenamientoObtenerComplemento(0,null);',function(err, rows){
            if (err){
                json.estado = "500";
                json.mensaje = "Se produjo un error al consultar todos los complementos";
            }else{   
                var complementos;
                var arr = [];
                json.estado = "200";
                json.mensaje = "OK";
                rows[0].forEach( (row) => { //recorre el arreglo que retorna la bd
                    complementos = new Object();
                    complementos.nombreComplemento= `${row.nombre}`;
                    complementos.idComplemento = `${row.idComplemento}`;
                    complementos.localicacionOriginal = `${row.LocalizacionOriginal}`;
                    complementos.nombre = `${row.nombreusr}`;
                    complementos.correo = `${row.correousr}`;
                    var arr_local = `${row.localizaciones}`.split(" ").join(","); //verifica si tiene solo un valor o tiene varios y lo separa en un arreglo
                    if(arr_local.length != 1){
                        var localizaciones = [];
                        arr_local = arr_local.split(",");
                        for(var i =0 ; i < arr_local.length; i++){                      
                            localizaciones.push(arr_local[i]); //se almacenan en un arreglo las localizaciones
                        };
                        complementos.localizaciones = localizaciones;
                    }else{
                        complementos.localizaciones = arr_local;
                    }
                    arr.push(complementos);
                });
                var comp = new Object();
                comp.complementos = arr;
                json.data = comp;
            }
            callback(null,json);
          });
    }
    getData(function(err, content){
        res.send(content);
    });   
});

        /**
        * @link http://IP:puerto/post/get/catalogo/{idComplemento}?token={token}
        * @description
        * Ruta que interactúan con la bd en donde obtiene un idCatalogo específico y devuelve su contenido de complemento
        */
app.get('/get/complemento/:idComplemento',function (request, res){
        const token = request.body.token || request.params.token || request.query.token;
        var validar = validation.tokenValidation(token)
        if(validar != true){
            return res.send(validar);
        }
        var json = new Object();
        function getData(callback){
            con.connecting().query('Call Almacenamiento.sp_almacenamientoObtenerComplementoId('+request.params.idComplemento+');',function(err, rows){
                if (err){
                    json.estado = "500";
                    json.mensaje = "Se produjo un error al consultar el complemento con el identificador";
                }else{ 
                    json.estado = "200";
                    json.mensaje = "OK";
                    var data;
                    rows[0].forEach( (row) => {
                        data = new Object();
                        data.nombreComplemento =`${row.nombre}`;
                        data.idComplemento =  `${row.idComplemento}`;
                        data.localizacionOriginal =  `${row.LocalizacionOriginal}`;
                        data.nombre = `${row.nombreusr}`;
                        data.correo = `${row.correousr}`;
                        //logica de localizaciones
                        var arr_localizaciones = `${row.localizaciones}`.split(" ").join(",");
                        if(arr_localizaciones.length != 1){
                            var localizaciones = [];
                            arr_localizaciones = arr_localizaciones.split(",");
                            for(var i =0 ; i < arr_localizaciones.length; i++){                      
                                localizaciones.push(arr_localizaciones[i]); //se almacenan en un arreglo las localizaciones
                            };
                            data.localizaciones = localizaciones;
                        }else{
                            data.localizaciones = arr_localizaciones;
                        }
                        //logica de cadenas
                        var arr_cadenas = `${row.Cadenas}`.split(" ").join(","); //verifica si tiene solo un valor o tiene varios y lo separa en un arreglo
                        if(arr_cadenas.length != 1){
                            var cadenas = [];
                            arr_cadenas = arr_cadenas.split(",");
                            for(var i =0 ; i < arr_cadenas.length; i++){                      
                                cadenas.push(arr_cadenas[i]); //se almacenan en un arreglo las localizaciones
                            };
                            data.cadenas = cadenas;
                        }else{
                            data.localizaciones = arr_cadenas;
                        }
                    });
                    json.data = data;
                }
                callback(null,json);
            });
        }
        getData(function(err, content){
            res.send(content);
        }); 
});
        /**
        * @link http://IP:puerto/delete/complemento/{idComplemento}?token={token}
        * @description
        * Ruta que interactúan con la bd y elimina un complemento por medio del ID
        */
app.get('/delete/complemento/:idComplemento', function (request, res){
    const token = request.body.token || request.params.token || request.query.token;
    var validar = validation.tokenValidation(token)
    if(validar != true){
        return res.send(validar);
    }
    var json = new Object();
    function getData(callback){
        con.connecting().query('Call Almacenamiento.sp_almacenamientoEliminaComplemento('+request.params.idComplemento+');',function(err, rows){
            if (err){
                json.estado = "500";
                json.mensaje = "Ha ocurrido un error al intentar eliminar el complemento";
            }else{ 
                json.estado = "200";
                json.mensaje = "Se ha eliminado el complemento satisfactoriamente";
            }
            callback(null,json);
        });
    }
    getData(function(err, content){
        res.send(content);
    }); 
});

        /**
        * @link http://IP:puerto/get/catalogo?token={token}
        * @description
        * Ruta que interactúan con la bd y devuelve el catálogo de internaciconalización
        */
app.get('/get/catalogo', function (request, res){
    const token = request.body.token || request.params.token || request.query.token;
    var validar = validation.tokenValidation(token)
    if(validar != true){
        return res.send(validar);
    }
    var json = new Object();
    function getData(callback){
        con.connecting().query('Call Almacenamiento.sp_almacenamientoObtenerCatalogo(null,null);',function(err, rows){
            if (err){
                json.estado = "500";
                json.mensaje = "Ha ocurrido un error al intentar leer Catálogo";
            }else{ 
                var catalogos;
                var idTemp = -1, idComp = 0; 
                var arr_catalogo = [];
                var contenido;
                var content;
                catalogos = new Object();
                var data = new Object();
                json.estado = "200";
                json.mensaje = "OK";
                var ban = 0;
                rows[0].forEach( (row) => { //recorre el arreglo que retorna la bd
                    if(ban == 0){
                        data.idComplemento = `${row.idComplemento}`;
                        data.nombre= `${row.Complemento}`;
                        ban = 1;
                    }
                    idComp = `${row.idComplemento}`;
                    if(idTemp != idComp){ //sino son iguales entra como primera vez
                        catalogos.localicacionOriginal = `${row.LocalizacionOriginal}`;
                        catalogos.localizacionTraduccion = `${row.LocalizacionTraduccion}`;
                        content = new Object();
                        content.msgid = `${row.msgid}`;
                        content.msgstr = `${row.msgstr}`;
                        content.numeroAprobaciones = `${row.numeroAprobaciones}`;
                        idTemp = idComp; //compararà todo el tiempo los id's diferentes al nuevo que venga
                        contenido = [];
                        contenido.push(content);
                        arr_catalogo.push(catalogos);
                        catalogos.contenido = contenido;    
                    }else{
                        content = new Object();
                        content.msgid = `${row.msgid}`;
                        content.msgstr = `${row.msgstr}`;
                        content.numeroAprobaciones = `${row.numeroAprobaciones}`;
                        contenido.push(content);

                    }
                });
                data.catalogo = catalogos;
                json.data = data;
            }
            callback(null,json);
        });
    }
    getData(function(err, content){
        res.send(content);
    });
});


        /**
        * @link http://IP:puerto/get/catalogo/:idComplemento?token={token}
        * @description
        * Ruta que interactúan con la bd y devuelve el catálogo de internaciconalización
        */


app.get('/get/catalogo/:idComplemento',function (request, res){
    const token = request.body.token || request.params.token || request.query.token;
    var validar = validation.tokenValidation(token)
    if(validar != true){
        return res.send(validar);
    }
    var json = new Object();
    function getData(callback){
        con.connecting().query('Call Almacenamiento.sp_almacenamientoObtenerCatalogo('+request.params.idComplemento+', null);',function(err, rows){
            if (err){
                json.estado = "500";
                json.mensaje = "Ha ocurrido un error al intentar buscar catálogo por ID";
            }else{ 
                json.estado = "200";
                json.mensaje = "OK";
                var data = new Object();
                var catalogo = new Object();
                var contenido = new Object();
                var contenido_arr = [];
                var ban = 0;
                if(rows[0].length != 0){
                    rows[0].forEach( (row) => { 
                        if(ban == 0){
                            data.nombreComplemento =`${row.Complemento}`;
                            data.idComplemento =  `${row.idComplemento}`;
                            catalogo.localizacionOriginal = `${row.localizacionOriginal}`;
                            catalogo.localizacionTraduccion=`${row.localizacionTraduccion}`;
                            ban = 1;
                        }
                        
                        contenido.msgid = `${row.msgid}`;
                        contenido.msgstr = `${row.msgstr}`;
                        contenido.numeroAprobaciones = `${row.numeroAprobaciones}`;
                        contenido_arr.push(contenido);
                    });
                    catalogo.contenido = contenido;
                    data.catalogo = catalogo;

                }else{
                    data.nombreComplemento ="";
                    data.idComplemento =  "";
                    data.localizacionOriginal ="";
                    data.localizacionTraduccion = "";
                    data.contenido = [];
                }
                json.data = data;
            }
            callback(null,json);
        });
    }
    getData(function(err, content){
        res.send(content);
    }); 
});

        /**
        * @link http://IP:puerto/post/agregarTraduccionCadena
        * @description
        * Ruta que almacena información de cadenas traducidas al almacenamiento
        */
app.post('/post/agregarTraduccionCadena',function (request, res){
    console.log('query:')
    console.log(request.query);
    console.log('params:')
    console.log(request.params);
    console.log('body:')
    console.log(request.body);
    const token = request.body.token || request.params.token || request.query.token;
    var validar = validation.tokenValidation(token)
    if(validar != true){
        return res.send(validar);
    }
    var json = new Object();
    function getData(callback){
        con.connecting().query('Call Almacenamiento.sp_almacenamientoInsertaTraduccion('+request.query.nombreComplemento+','+
        +request.query.idComplemento+','+request.query.nombreLocalizacionOriginal+','+ request.query.nombreLocalizacionTraducida+','+
        request.query.cadenaOriginal+','+request.query.cadenaTraducida+','+request.query.nombre+','+request.query.correo+',1);',function(err, rows){
            if (err){
                json.estado = "500";
                json.mensaje = "Ha ocurrido un error al intentar buscar catálogo por ID";
            }else{ 
                json.estado = "200";
                json.mensaje = "La cadena traducida se ha almacenado exitósamente";
            }
            callback(null,json);
        });
    }
    getData(function(err, content){
        res.send(content);
    });   
});

        /**
        * @link http://IP:puerto/post/aprobarTraduccionCadena
        * @description
        * Ruta que almacena información de cadenas traducidas al almacenamiento
        */
    app.post('/post/aprobarTraduccionCadena',function (request, res){
        const token = request.body.token || request.params.token || request.query.token;
        var validar = validation.tokenValidation(token)
        if(validar != true){
            return res.send(validar);
        }
        var json = new Object();
        function getData(callback){
            con.connecting().query('Call Almacenamiento.sp_almacenamientoInsertaTraduccion('+
            request.query.nombreComplemento+','+request.query.idComplemento+','+request.query.nombreLocalizacionOriginal+','+ request.query.nombreLocalizacionTraducida+','+
            request.query.cadenaOriginal+','+request.query.cadenaTraducida+','+request.query.nombre+','+request.query.correo+',2);',function(err, rows){
                if (err){
                    json.estado = "500";
                    json.mensaje = "Ha ocurrido un error al intentar buscar catálogo por ID";
                }else{ 
                    if(rows[0].length != 0){
                        rows[0].forEach( (row) => { 
                            var estado =`${row.estado}`;
                            if (estado  == "404"){
                                json.estado = "404";
                                json.mensaje = "Error al insertar la aprobación traducción de cadena ya existen 2 aprobaciones.";
                            }else{
                                json.estado = "200";
                                json.mensaje = "La cadena aprobada se ha almacenado exitósamente";                            
                            }
                        });
                    }
                }
                callback(null,json);
        });
    }
        getData(function(err, content){
            res.send(content);
        });   
    });



        /**
        * @link http://IP:puerto/post/suscripcion?token={token}
        * @description
        * Ruta que interactúan con la BD en donde retorna la lista de carácteres
        */
app.post('/post/suscripcion',function (request, res){
    const token = request.body.token || request.params.token || request.query.token;
    var validar = validation.tokenValidation(token)
    if(validar != true){
        return res.send(validar);
    }
    var IP = request.query.ip;
    var json = new Object();
    function getData(callback){
        con.connecting().query('Call Almacenamiento.sp_almacenamientosuscripcion("'+IP+'");',function(err, rows){
            if (err){
                json.codigo = "500";
                json.mensaje = "Ha ocurrido un error al suscribirse";
            }else{ 
                json.codigo = "200";
                json.mensaje = "Suscrito correctamente";
            }
            callback(null,json);
        });
    }
    getData(function(err, content){
        res.send(content);
    }); 

});


        /**
        * @link http://IP:puerto//get/localizacion/:idUsuario?token={token}
        * @description
        * Ruta que interactúan con la BD en donde retorna la lista de localizaciones
        */
app.get('/get/localizacion/:idUsuario',function (request, res){
    const token = request.body.token || request.params.token || request.query.token;
    var validar = validation.tokenValidation(token)
    if(validar != true){
        return res.send(validar);
    }
    var json = new Object();
    function getData(callback){
        con.connecting().query('Call Almacenamiento.sp_almacenamientoObtenerLocalizacion();',function(err, rows){
            if (err){
                json.estado = "500";
                json.mensaje = "Ha ocurrido un error al obtener lista de complementos";
            }else{ 
                json.estado = "200";
                json.mensaje = "OK";
                var localizaciones = [];
                rows[0].forEach( (row) => {
                    localizaciones.push(`${row.localizacion}`);
                });
                json.data = localizaciones;
            }
            callback(null,json);
        });
    }
    getData(function(err, content){
        res.send(content);
    }); 

});


        /** PENDIENTE REVISAR
        * @link http://IP:puerto/post/complementoTraducido
        * @description
        * Ruta que envia información de cadenas limpias al microservicio de traducidos
        */
    app.post('/post/complementoTraducido',function (request, res){
        const token = request.body.token || request.params.token || request.query.token;
        var validar = validation.tokenValidation(token)
        if(validar != true){
            return res.send(validar);
        }
        var json = new Object();
        function getData(callback){
            con.connecting().query('Call Almacenamiento.sp_traducidoscomplementoTraducido();',function(err, rows){
                if (err){
                    json.estado = "500";
                    json.mensaje = "Ha ocurrido un error al intentar buscar catálogo por ID";
                }else{ 
                    var complemento  = new Object();
                    var contenido = new Object();
                    var arr_contenido =  [];
                    var arr_complemento =  [];
                    var ban = 0;
                    var idComp, idTemp = -1;
                    rows[0].forEach( (row) => {
                        if(ban == 0){
                            json.nombre = `${row.nombre}`;
                            json.correo = `${row.correo}`;
                            ban = 1;
                        }
                        idComp = `${row.idComplemento}`;
                        if(idComp != idTemp){
                            complemento.nombreComplemento = `${row.nombreComplemento}`;
                            complemento.localizacionOriginal = `${row.localizacionoriginal}`;
                            complemento.localizacionTraducida = `${row.localizaciontraducida}`;
                            idTemp = idComp;
                            contenido = new Object(); 
                            contenido.msgid = `${row.msgid}`;
                            contenido.msgstr = `${row.msgstr}`;
                            arr_contenido =  [];
                            arr_contenido.push(contenido);
                            arr_complemento.push(complemento);
                            complemento.contenido = arr_contenido;
                        }else{  
                            contenido = new Object(); 
                            contenido.msgid = `${row.msgid}`;
                            contenido.msgstr = `${row.msgstr}`;
                            arr_contenido.push(contenido);
                        }
                    });
                    json.complemento = complemento;
                }
                callback(null,json);
            });
        }
        getData(function(err, content){
            res.send(content);
        });   
    });


module.exports = app




