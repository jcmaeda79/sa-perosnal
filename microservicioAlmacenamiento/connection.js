var mysql      = require('mysql');

function connecting(){
  var connection = mysql.createConnection({
    host     : 'mysqldb',
    port     : process.env.MYSQL_PORT,
    user     : process.env.MYSQL_USER,
    password : process.env.MYSQL_PASSWORD,
    database : process.env.MYSQL_DATABASE,
    insecureAuth : true
  });
  connection.connect();
  return connection;
} 

//connection.end();
console.log('conectado...')
module.exports = {
  connecting
}