CREATE DEFINER=`root`@`%` PROCEDURE `sp_almacenamientoInsertaComplemento`(IN nombreusr varchar(100),IN correousr varchar(150),IN icomplemento varchar(50),IN ilocalizacion varchar(15),_cadena varchar(6500))
BEGIN
	declare vaidEstado int;
    declare vaidLocalizacion int;
    declare vaidComplemento int;
    declare Ruta varchar(500);
         
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
	  BEGIN
			select 401 estado, 'Error al crear complemento' mensaje;				 	          
	  END;
     
     select IdEstado INTO vaidEstado from Almacenamiento.estado where Estado = 'Activo';
    if (select count(*) from Almacenamiento.Complemento where Complemento = iComplemento) = 0 then
		insert into Almacenamiento.Complemento (Complemento,nombreusr,correousr,IdEstado) values (icomplemento,nombreusr,correousr,vaidEstado);   
	end if;
    set Ruta = null;
    select idComplemento INTO vaidComplemento from Almacenamiento.Complemento where Complemento = iComplemento;
    select idlocalizacion INTO vaidLocalizacion from Almacenamiento.Localizacion where localizacion = ilocalizacion;
    insert into Almacenamiento.ComplementoLocalizacion (idComplemento,idLocalizacion,idEstado,Ruta) 
    values(vaidComplemento,vaidLocalizacion,vaidEstado,Ruta);
    
    truncate table table1;
	insert into table1 (value) values(_cadena);
	
    insert into Cadena (idComplemento,idLocalizacion,idEstado,nombreusr,correousr,cadena)
	SELECT
	  vaidComplemento,vaidLocalizacion,vaidEstado,nombreusr,correousr,
	  SUBSTRING_INDEX(SUBSTRING_INDEX(value, ',', n.digit+1), ',', -1) Valor
	FROM
	  table1
	  INNER JOIN
	  (SELECT 0 digit UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3) n
	  ON LENGTH(REPLACE(value, ',' , '')) <= LENGTH(value)-n.digit
	ORDER BY
	  id,
	  n.digit;

    
    
        
    if (select count(*) from Almacenamiento.Complemento where Complemento = Complemento) > 0 then
    	select '200' Estado, 'OK' Mensaje, concat('Se Inserto correctamente el Complemento ', iComplemento) Nombre;
	end if;    
END