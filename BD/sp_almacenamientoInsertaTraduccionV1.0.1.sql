CREATE DEFINER=`root`@`%` PROCEDURE `sp_almacenamientoInsertaTraduccion`(
_Complemento varchar(60),_idComplementov int,_LocalizacionOriginal varchar(10),_Localizacion varchar(10),
	_cadenaOrginal varchar(16383),_cadena varchar(16383),_nombreusr varchar(100),_correousr varchar(150),
    _opcion int
)
BEGIN
	  declare _idComplemento int;
      declare _idLocalizacion int;
      declare _idCadenaOriginal int;
      declare _idestado int;
      declare _iddetallecomplemento int;
      declare _idLocalizacionOriginal int;
      DECLARE EXIT HANDLER FOR SQLEXCEPTION
	  BEGIN
		select 401 estado,'Error al insertar la traduccion ' mensaje;               
	  END;
		set _idComplemento = _idComplementov;
		set _idComplemento = (select idComplemento from Complemento where Complemento = _Complemento);
		set _idLocalizacion = (select idLocalizacion from Localizacion where Localizacion = _Localizacion);
		set _idLocalizacionOriginal = (select idLocalizacion from Localizacion where Localizacion = _LocalizacionOriginal);
		set _idestado = (select idestado from estado where Estado = 'Traducido');
		set _idCadenaOriginal = (select idDetalleComplemento from Cadena 
								  where idComplemento = _idComplemento 
								  and idLocalizacion = _idLocalizacionOriginal
								  and cadena = _cadenaOrginal and idCadenaOriginal is null);
      
      /*Se puede utilizar el mismo procedimiento para insertar y para aprobar una traduccion,
        el parametro opcion 1 para insertar una traducicon, y 0 para aprobarlas*/
		/*Hace las busquedas de los identificadores*/
        if _opcion = 1 then
        			
			insert into Cadena (idComplemento,idLocalizacion,idEstado,cadena,nombreusr,correousr,idCadenaOriginal)
			values (_idComplemento,_idLocalizacion,_idestado,_cadena,_nombreusr,_correousr,_idCadenaOriginal);
			
			set _iddetallecomplemento = (select iddetallecomplemento from Cadena 
											where idComplemento = _idComplemento
											and idEstado = _idestado
											and cadena = _cadena
											and idLocalizacion = _idLocalizacion);
										   
			/*insert into BitacoraCambio (idDetalleComplemento,IdEstado,FechaCambio,nombreusr,correousr)
			values (_iddetallecomplemento,_idestado,now(),_nombreusr,_correousr);
			*/
			select 200 estado, 'Traduccion insertada correctamente';
		else
			set _iddetallecomplemento = (select iddetallecomplemento 
										 from Cadena 
										 where idComplemento = _idComplemento
											and idEstado = _idestado
											and cadena = _cadena
											and idLocalizacion = _idLocalizacion);
			set _idestado = (select idestado from estado where Estado = 'Aprobado');
            select 200 estado, concat('Aprobacion de traduccion ',_cadena) mensaje;
		end if;
        insert into BitacoraCambio (idDetalleComplemento,IdEstado,FechaCambio,nombreusr,correousr)
		values (_iddetallecomplemento,_idestado,now(),_nombreusr,_correousr);
END